package main

import (
	"github.com/mattn/go-gtk/glib"
	"github.com/mattn/go-gtk/gtk"
)

type TrayIcon struct {
	Window
	menu     *gtk.Menu
	settings *gtk.MenuItem
	quit     *gtk.MenuItem
}

func (t *TrayIcon) Init() {
	t.menu = gtk.NewMenu()
	t.quit = gtk.NewMenuItemWithLabel("Išeiti")
	t.quit.Connect("activate", func() {
		gtk.MainQuit()
	})
	t.menu.Append(t.quit)
	si := gtk.NewStatusIconFromStock(gtk.STOCK_FLOPPY)
	si.SetTitle("Debesų saugyklų sinchronizavimo sistema")
	si.SetTooltipMarkup("Debesų saugyklų sinchronizavimo sistema")
	si.Connect("popup-menu", func(cbx *glib.CallbackContext) {
		t.menu.Popup(nil, nil, gtk.StatusIconPositionMenu, si, uint(cbx.Args(0)), uint32(cbx.Args(1)))
	})
	t.SettingsMenu()
}

func (t *TrayIcon) clear() {
	if t.settings != nil {
		t.settings.Destroy()
	}
}

func (t *TrayIcon) SettingsMenu() {
	t.clear()
	t.settings = gtk.NewMenuItemWithLabel("Nustatymai")
	t.settings.Connect("activate", func() {
		t.Mediator.OpenSettings()
	})
	t.menu.Prepend(t.settings)
	t.menu.ShowAll()
}
