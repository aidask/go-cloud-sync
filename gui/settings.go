package main

import (
	"time"

	"golang.org/x/oauth2"

	"bitbucket.org/aidask/go-cloud-sync/lib"
	"github.com/mattn/go-gtk/gdk"
	"github.com/mattn/go-gtk/gtk"
	"github.com/skratchdot/open-golang/open"
)

const (
	DefaultGoogleClientId     = "86758202287-0v80bb7019tbnh6ttqvmo7lhusn1hlc5.apps.googleusercontent.com"
	DefaultGoogleClientSecret = "_IfUC3Y3C4xvj_NztTZa0IAF"
)

type SettingsWindow struct {
	Window
	window *gtk.Window
}

func (w *SettingsWindow) Open() {
	if w.window != nil {
		w.window.Destroy()
	}
	w.window = gtk.NewWindow(gtk.WINDOW_TOPLEVEL)
	w.window.SetPosition(gtk.WIN_POS_CENTER)
	w.window.SetTitle("Nustatymai")
	w.window.SetIconName("gtk-dialog-info")

	connectedLabel := gtk.NewLabel("Prijungtų saugyklų sąrašas")
	connectedBox := gtk.NewComboBoxText()
	connected := w.Mediator.sync.sync.Storages()
	removeMap := []int{}
	for _, c := range connected {
		connectedBox.AppendText(c.Name())
		removeMap = append(removeMap, c.Id())
	}
	remove := gtk.NewButtonWithLabel("Ištrinti")
	remove.Clicked(func() {
		if connectedBox.GetActive() == -1 {
			return
		}
		id := removeMap[connectedBox.GetActive()]
		connectedStorage := &lib.ConnectedStorage{}
		err := w.Mediator.db.Where("id=?", id).First(connectedStorage).Error
		w.Mediator.CheckErr(err)
		c := lib.NewStorageController(w.Mediator.db, connectedStorage)
		err = c.Delete()
		w.Mediator.CheckErr(err)
		w.Mediator.sync.RemoveStorage(id)
		w.Open()
	})
	removeBox := gtk.NewHBox(false, 1)
	removeBox.Add(connectedBox)
	removeBox.Add(remove)

	addLabel := gtk.NewLabel("Prijungti saugyklą")
	storagesBox := gtk.NewComboBoxText()
	storages := w.Mediator.storagesList()
	for _, storage := range storages {
		if storage.Id == lib.StorageFolder || storage.Id == lib.StorageDummy {
			continue
		}
		storagesBox.AppendText(storage.Name)
	}
	add := gtk.NewButtonWithLabel("Prijungti")
	add.Clicked(func() {
		switch storagesBox.GetActive() + 1 {
		case lib.StorageGoogleDrive:
			err := w.connectGoogleDrive()
			w.Mediator.CheckErr(err)
		}
	})
	hBox := gtk.NewHBox(true, 1)
	hBox.Add(storagesBox)
	hBox.Add(add)

	startSync := gtk.NewButtonWithLabel("Pradėti sinchronizavimą")
	startSync.Clicked(func() {
		if len(w.Mediator.connectedList()) < 2 {
			w.Mediator.Alert("Turite pridėti bent vieną saugyklą, kad pradėtumėte sinchronizavimą")
		} else {
			w.Mediator.sync.Start()
		}
	})
	stopSync := gtk.NewButtonWithLabel("Sustabdyti sinchronizaciją")
	stopSync.Clicked(func() {
		w.Mediator.sync.Stop()
	})
	stateLabel := gtk.NewLabel("Sinchronizavimo būsena:")
	stateLabel.SetAlignment(1, 0.5)
	state := gtk.NewLabel(w.Mediator.sync.State())
	stateBox := gtk.NewHBox(true, 1)
	stateBox.Add(stateLabel)
	stateBox.Add(state)
	progress := gtk.NewProgressBar()
	progress.SetFraction(0.5)

	vbox := gtk.NewVBox(false, 3)
	vbox.Add(connectedLabel)
	vbox.Add(removeBox)
	vbox.Add(addLabel)
	vbox.Add(hBox)
	vbox.Add(stateBox)
	vbox.Add(progress)
	vbox.Add(stopSync)
	vbox.Add(startSync)

	w.window.Add(vbox)
	w.window.SetSizeRequest(400, 300)
	w.window.ShowAll()
	time.Sleep(1 * time.Second)
	go func() {
		run := true
		w.window.Connect("destroy", func() {
			run = false
		})
		sync := w.Mediator.sync
		for run {
			gdk.ThreadsEnter()
			if sync.progress == nil {
				progress.SetFraction(0)
			} else {
				progress.Pulse()
				progress.SetFraction(sync.progress.Percent())
			}
			if sync.running {
				stopSync.SetVisible(true)
				startSync.SetVisible(false)
			} else {
				stopSync.SetVisible(false)
				startSync.SetVisible(true)
			}
			state.SetText(sync.State())
			gdk.ThreadsLeave()
			time.Sleep(200 * time.Millisecond)
		}
	}()
}
func (w *SettingsWindow) connectGoogleDrive() error {
	credentials := &lib.GoogleDriveApiCredentials{
		ClientID:     DefaultGoogleClientId,
		ClientSecret: DefaultGoogleClientSecret,
	}
	cs := &lib.ConnectedStorage{StorageId: lib.StorageGoogleDrive}
	cs.WriteCredentials(credentials)
	err := w.newGoogleDriveSync(cs)
	return err
}
func (w *SettingsWindow) newGoogleDriveSync(cs *lib.ConnectedStorage) error {
	factory, err := lib.NewGoogleDriveSyncFactory(w.Mediator.db)
	factory.Debug = w.Mediator.debug
	if err != nil {
		return err
	}
	csc := lib.NewStorageController(w.Mediator.db, cs)
	cr := &lib.GoogleDriveApiCredentials{}
	csc.Credentials(cr)
	if !cr.IsAuthenticated() {
		conf := factory.CreateOauth2Config(cr)
		url := conf.AuthCodeURL("state", oauth2.AccessTypeOffline)

		w.Prompt(url, func(code string, folder string) {
			if code == "" {
				return
			}
			tok, err := conf.Exchange(oauth2.NoContext, code)
			if err != nil {
				w.Mediator.Alert("Prisijungimas nepavyko. \n\n" + err.Error())
				return
			}
			cr.Token = tok
			cr.BaseFolderName = folder
			csc.SaveCredentials(cr)
			s, err := factory.Create(csc)
			w.Mediator.CheckErr(err)
			w.Mediator.sync.AddStorage(s)
		})
	}
	return nil
}
func (w *SettingsWindow) Prompt(url string, cb func(response string, folder string)) {
	if w.window != nil {
		w.window.Destroy()
	}
	w.window.Destroy()
	window := gtk.NewWindow(gtk.WINDOW_TOPLEVEL)
	w.window = window
	window.SetPosition(gtk.WIN_POS_CENTER)
	window.SetTitle("Settings")
	window.SetIconName("gtk-dialog-info")

	text := gtk.NewLabel("Prisijunkite prie saugyklos ir nukopijuokite\n prisijungimo kodą į žemiau pateiktą laukelį")
	text.SetJustify(gtk.JUSTIFY_CENTER)
	copyUrl := gtk.NewButtonWithLabel("Prisijungti")
	copyUrl.Clicked(func() {
		open.Start(url)
	})
	codeLabel := gtk.NewLabel("Prisijungimo kodas:")
	code := gtk.NewEntry()

	folderLabel := gtk.NewLabel("Sinchronizuojamo aplanko pavadinimas:")
	folder := gtk.NewEntry()
	folder.SetText("Sync")

	buttons := gtk.NewHBox(true, 1)

	ok := gtk.NewButtonWithLabel("Išsaugoti")
	ok.Clicked(func() {
		cb(code.GetText(), folder.GetText())
		w.Open()
	})
	cancel := gtk.NewButtonWithLabel("Atšaukti")
	cancel.Clicked(func() {
		cb("", "")
		w.Open()
	})
	buttons.Add(ok)
	buttons.Add(cancel)

	vbox := gtk.NewVBox(false, 3)
	vbox.Add(text)
	vbox.Add(copyUrl)
	vbox.Add(codeLabel)
	vbox.Add(code)
	vbox.Add(folderLabel)
	vbox.Add(folder)
	vbox.Add(buttons)

	window.Add(vbox)
	window.SetSizeRequest(400, 300)
	window.ShowAll()
}
