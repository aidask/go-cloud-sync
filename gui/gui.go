package main

import (
	"errors"
	"fmt"
	"os"
	"time"

	"bitbucket.org/aidask/go-cloud-sync/lib"
	_ "github.com/mattn/go-sqlite3"

	"github.com/jinzhu/gorm"
	"github.com/mattn/go-gtk/gdk"
	"github.com/mattn/go-gtk/glib"
	"github.com/mattn/go-gtk/gtk"
)

type FolderSyncGuiFactory struct {
	*lib.FolderSyncFactory
}

type Mediator struct {
	trayIcon       *TrayIcon
	settingsWindow *SettingsWindow
	window         *Window
	path           string
	debug          bool
	db             *gorm.DB
	folderSync     lib.ServiceSyncer
	sync           *SyncController
}

func (m *Mediator) Init(args []string) {
	if len(args) == 2 {
		m.InitPath(args[1])
		return
	}
	filechooserdialog := gtk.NewFileChooserDialog(
		"Pasirinkite aplanką sinchronizacijai",
		nil,
		gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,
		gtk.STOCK_OK,
		gtk.RESPONSE_ACCEPT)
	filechooserdialog.Response(func() {
		path := filechooserdialog.GetFilename()
		filechooserdialog.Destroy()
		if path == "" {
			m.window.AlertError("Nepasirinktas aplankas.")
			gtk.MainQuit()
		} else {
			m.InitPath(path)
		}
	})
	filechooserdialog.Show()
}

func (m *Mediator) storagesList() []lib.Storage {
	storages := []lib.Storage{}
	m.CheckErr(m.db.Find(&storages).Error)
	return storages
}
func (m *Mediator) connectedList() []lib.ConnectedStorage {
	cs := []lib.ConnectedStorage{}
	m.CheckErr(m.db.Preload("Storage").Find(&cs).Error)
	return cs
}

func (m *Mediator) InitPath(path string) {
	s, err := os.Stat(path)
	m.CheckErr(err)
	if !s.IsDir() {
		m.Fatal(errors.New("Synchronization path is not a directory"))
	}
	m.path = lib.NormalizeDirPath(path)
	m.InitDb()
	m.InitMainStorage()
	m.InitSync()
	m.trayIcon.Init()
	if len(m.connectedList()) < 2 {
		m.settingsWindow.Open()
	} else {
		m.CheckErr(m.sync.Start())
	}
}
func (m *Mediator) InitDb() {
	db, err := gorm.Open("sqlite3", m.path+".cloudsync.db")
	m.CheckErr(err)
	db.LogMode(m.debug)
	m.db = &db
	err = lib.InitDb(&db)
	m.CheckErr(err)
}
func (m *Mediator) InitMainStorage() {
	folderSync := m.NewFolderSync()
	m.folderSync = folderSync
}
func (m *Mediator) InitSync() {
	sync, err := lib.NewSync(m.db, m.folderSync, m.debug)
	m.sync = &SyncController{sync: sync}
	m.CheckErr(err)
}
func (m *Mediator) NewFolderSync() lib.ServiceSyncer {
	factory, err := lib.NewFolderSyncFactory(m.db, m.debug)
	m.CheckErr(err)
	factory.Debug = m.debug
	cons := &FolderSyncGuiFactory{
		FolderSyncFactory: factory,
	}
	cs := &lib.ConnectedStorage{}
	m.db.Where(lib.ConnectedStorage{StorageId: lib.StorageFolder}).Preload("Storage").FirstOrInit(cs)
	err = cs.WriteCredentials(&lib.FolderSyncCredentials{
		Path: m.path,
	})
	m.CheckErr(err)
	if m.db.NewRecord(cs) {
		err = m.db.Save(cs).Error
		m.CheckErr(err)
	}
	csc := lib.NewStorageController(m.db, cs)
	s, err := cons.Create(csc)
	m.CheckErr(err)
	return s
}

func (m *Mediator) CheckErr(e error) {
	if e != nil {
		m.Fatal(e)
	}
}
func (m *Mediator) Fatal(e error) {
	m.window.Fatal(e)
	panic(e)
}
func (m *Mediator) Alert(t string) {
	m.window.AlertWarning(t)
}
func (m *Mediator) OpenSettings() {
	m.settingsWindow.Open()
}

type SyncController struct {
	sync     *lib.Sync
	stop     chan bool
	running  bool
	progress *lib.Progress
}

func (s *SyncController) State() string {
	if !s.running {
		return "Sustabdyta"
	}
	if s.progress != nil {
		return "Sinchronizuoja..."
	}
	return "Laukia atnaujinimų"
}
func (s *SyncController) AddStorage(ss lib.ServiceSyncer) {
	run := s.running
	s.Stop()
	s.sync.AddStorage(ss)
	if run {
		s.Start()
	}
}
func (s *SyncController) RemoveStorage(id int) {
	run := s.running
	s.Stop()
	s.sync.RemoveStorageById(id)
	if run {
		s.Start()
	}
}

func (s *SyncController) Start() error {
	if s.running {
		return errors.New("Sinchronizacija jau paleista")
	}
	s.stop = make(chan bool)
	s.running = true
	go func() {
		for {
			fmt.Print(".")
			select {
			case <-s.stop:
				s.running = false
				close(s.stop)
				return
			default:
			}
			err := s.Sync()
			if err != nil {
				panic(err)
			}
			time.Sleep(time.Duration(5) * time.Second)
		}
	}()
	return nil
}
func (s *SyncController) Stop() {
	if !s.running {
		return
	}
	s.stop <- true
	<-s.stop //Block for closing
}
func (s *SyncController) Sync() error {
	changec := make(chan *lib.FileChange)
	progressc := make(chan *lib.Progress)
	hasChanges := false
	go func() {
		err := s.sync.Sync(changec, progressc)
		if err != nil {
			panic(err)
		}
	}()
Loop:
	for {
		select {
		case c := <-changec:
			if !hasChanges {
				fmt.Println("\nChange list:")
			}
			hasChanges = true
			fmt.Printf("  * %s %s (%s -> %s)\n", c.Type(), c.File.Path, c.Source.Name(), c.Target.Name())
		case p, ok := <-progressc:
			if !ok {
				break Loop
			}
			s.progress = p
		}
	}
	s.progress = nil
	return nil
}

func main() {
	m := &Mediator{}
	w := Window{m, nil}
	m.window = &w
	m.settingsWindow = &SettingsWindow{Window: w}
	m.trayIcon = &TrayIcon{Window: w}

	gtk.Init(&os.Args)
	gdk.ThreadsInit()
	glib.SetApplicationName("Debesų saugyklų sinchronizavimo sistema")

	args := os.Args
	// args := []string{"", "test"}
	m.Init(args)
	gdk.ThreadsEnter()
	gtk.Main()
	gdk.ThreadsLeave()
}
