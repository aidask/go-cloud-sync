package main

import (
	"fmt"
	"log"

	"github.com/mattn/go-gtk/gtk"
)

type Window struct {
	Mediator *Mediator
	parent   *gtk.Window
}

func (w *Window) Alert(message string, t gtk.MessageType) {
	messagedialog := gtk.NewMessageDialog(
		w.parent,
		gtk.DIALOG_MODAL,
		t,
		gtk.BUTTONS_OK,
		message)
	messagedialog.Response(func() {
		messagedialog.Destroy()
	})
	messagedialog.Run()
}
func (w *Window) AlertWarning(message string) {
	w.Alert(message, gtk.MESSAGE_WARNING)
}
func (w *Window) AlertError(message string) {
	w.Alert(message, gtk.MESSAGE_ERROR)
}
func (w *Window) Fatal(e error) {
	w.AlertError(fmt.Sprintf("Oooops... \nSorry for inconvenience, application has encountered an unexpected error: \n\n%s", e.Error()))
	log.Fatal(e)
}
