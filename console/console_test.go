package main

import (
	"os"
	"testing"
)

var syncPath string

func initSyncDir() {
	wd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	syncPath = wd + "/test/"
	err = os.RemoveAll(syncPath)
	if err != nil {
		panic(err)
	}
	err = os.Mkdir(syncPath, 0777)
	if err != nil {
		panic(err)
	}
}

func TestMain(m *testing.M) {
	initSyncDir()
	os.Exit(m.Run())
}

func ExampleVersion() {
	initSyncDir()
	console().Run([]string{"console", "-v"})
	// Output:
	// console version 0.1
}
func ExampleHelp() {
	initSyncDir()
	run("help")
}
func ExampleAddMissing() {
	initSyncDir()
	run("add", "No such")
	// Output:
	// Record was not found
}
func ExampleAdd() {
	initSyncDir()
	run("add", "Dummy")
	// Output:
	// Storage was added
}
func ExampleList() {
	initSyncDir()
	run("list")
	// Output:
	// Got 1 storages
	// #1 Folder
}
func ExampleListWithAdd() {
	initSyncDir()
	run("add", "Dummy")
	run("list")
	// Output:
	// Storage was added
	// Got 2 storages
	// #1 Folder
	// #2 Dummy
}
func ExampleRemove() {
	initSyncDir()
	run("remove", "22")
	// Output:
	// Record was not found
}
func ExampleRemoveWithAdd() {
	initSyncDir()
	run("add", "Dummy")
	run("remove", "2")
	// Output:
	// Storage was added
	// Storage was removed
}
func ExampleChanges() {
	initSyncDir()
	run("changes")
	// Output:
}
func ExampleChangesNewFile() {
	initSyncDir()
	touch("file1")
	run("changes")
	// Output:
	// * create file1 (#1 Folder -> ?)
}
func ExampleSync() {
	initSyncDir()
	run("add", "Dummy")
	touch("file1")
	run("sync")
	// Output:
	// Storage was added
	// Changes list:
	//  * create file1 (#1 Folder -> #2 Dummy)
}
func ExampleSyncRemove() {
	initSyncDir()
	run("add", "Dummy")
	touch("file1")
	run("sync")
	remove("file1")
	run("sync")
	// Output:
	// Storage was added
	// Changes list:
	//  * create file1 (#1 Folder -> #2 Dummy)
	// Changes list:
	//  * remove file1 (#1 Folder -> #2 Dummy)
}
func ExampleSyncUpdate() {
	initSyncDir()
	run("add", "Dummy")
	touch("file1")
	run("sync")
	write("file1", "1")
	run("sync")
	// Output:
	// Storage was added
	// Changes list:
	//  * create file1 (#1 Folder -> #2 Dummy)
	// Changes list:
	//  * update file1 (#1 Folder -> #2 Dummy)
}

func run(args ...string) {
	args = append([]string{"console", "-p", "./test/"}, args...)
	console().Run(args)
}
func touch(name string) {
	h, err := os.Create(syncPath + name)
	if err != nil {
		panic(err)
	}
	h.Close()
}
func write(name string, content string) {
	h, err := os.Create(syncPath + name)
	if err != nil {
		panic(err)
	}
	defer h.Close()
	_, err = h.WriteString(content)
	if err != nil {
		panic(err)
	}
}
func remove(name string) {
	err := os.Remove(syncPath + name)
	if err != nil {
		panic(err)
	}
}
