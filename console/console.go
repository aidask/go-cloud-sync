package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/aidask/go-cloud-sync/lib"
	"github.com/cheggaaa/pb"
	"github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3"

	"golang.org/x/oauth2"

	"github.com/codegangsta/cli"
)

const (
	DefaultGoogleClientId     = "86758202287-0v80bb7019tbnh6ttqvmo7lhusn1hlc5.apps.googleusercontent.com"
	DefaultGoogleClientSecret = "_IfUC3Y3C4xvj_NztTZa0IAF"
)

type FolderSyncConsoleFactory struct {
	*lib.FolderSyncFactory
}

type GoogleDriveSyncConsoleFactory struct {
	*lib.GoogleDriveSyncFactory
}

func (c *GoogleDriveSyncConsoleFactory) Create(csc *lib.ConnectedStorageController) (lib.ServiceSyncer, error) {
	cr := &lib.GoogleDriveApiCredentials{}
	csc.Credentials(cr)
	if !cr.IsAuthenticated() {
		conf := c.CreateOauth2Config(cr)
		url := conf.AuthCodeURL("state", oauth2.AccessTypeOffline)

		fmt.Printf("Visit the URL for the auth dialog: %v\n", url)
		var code string
		if _, err := fmt.Scan(&code); err != nil {
			return nil, err
		}
		tok, err := conf.Exchange(oauth2.NoContext, code)
		if err != nil {
			return nil, err
		}
		cr.Token = tok
		fmt.Println("Enter folder name where files will be synced to: ")
		if _, err := fmt.Scan(&cr.BaseFolderName); err != nil {
			return nil, err
		}
		csc.SaveCredentials(cr)
	}
	return c.GoogleDriveSyncFactory.Create(csc)
}

func checkStorageErr(err error, sync lib.ServiceSyncer) {
	if err == nil {
		return
	}
	fmt.Printf("Could not connect to storage, reason: \n%s\n\n Destroy storage? [y/n]", err)
	var destroy string
	_, err = fmt.Scan(&destroy)
	checkErr(err)
	if destroy == "y" {
		err = sync.Destroy()
		checkErr(err)
	}
}

func checkErr(err error) {
	if err != nil {
		log.Fatalf("Error: %v\n", err)
	}
}

type ConsoleController struct {
	db         *gorm.DB
	debug      bool
	path       string
	folderSync lib.ServiceSyncer
}

func NewConsoleController(c *cli.Context) *ConsoleController {
	var err error
	debug := c.GlobalBool("debug")
	path := c.GlobalString("path")
	if path == "" {
		path, err = os.Getwd()
		checkErr(err)
	}
	cc := &ConsoleController{
		path:  path,
		debug: debug,
	}
	err = cc.InitPath()
	checkErr(err)
	err = cc.InitDb()
	checkErr(err)
	err = cc.InitMainStorage()
	checkErr(err)
	return cc
}
func (c *ConsoleController) InitPath() error {
	s, err := os.Stat(c.path)
	if err != nil {
		return err
	}
	if !s.IsDir() {
		return errors.New("Synchronization path is not a directory")
	}
	c.path = lib.NormalizeDirPath(c.path)
	if c.debug {
		fmt.Printf("Synchronization directory %s\n", c.path)
	}
	return nil
}
func (c *ConsoleController) InitDb() error {
	db, err := gorm.Open("sqlite3", c.path+".cloudsync.db")
	if err != nil {
		return err
	}
	db.LogMode(c.debug)
	c.db = &db
	return lib.InitDb(c.db)
}
func (c *ConsoleController) InitMainStorage() error {
	folderSync, err := c.NewFolderSync()
	if err != nil {
		return err
	}
	c.folderSync = folderSync
	return nil
}
func (c *ConsoleController) NewFolderSync() (lib.ServiceSyncer, error) {
	factory, err := lib.NewFolderSyncFactory(c.db, c.debug)
	if err != nil {
		return nil, err
	}
	factory.Debug = c.debug
	cons := &FolderSyncConsoleFactory{
		FolderSyncFactory: factory,
	}
	cs := &lib.ConnectedStorage{}
	c.db.Where(lib.ConnectedStorage{StorageId: lib.StorageFolder}).Preload("Storage").FirstOrInit(cs)
	err = cs.WriteCredentials(&lib.FolderSyncCredentials{
		Path: c.path,
	})
	if c.db.NewRecord(cs) {
		if err != nil {
			return nil, err
		}
		err = c.db.Save(cs).Error
		if err != nil {
			return nil, err
		}
		c.db.Model(cs).Related(&cs.Storage)
	}
	csc := lib.NewStorageController(c.db, cs)
	return cons.Create(csc)
}
func (c *ConsoleController) NewGoogleDriveSync(cs *lib.ConnectedStorage) (lib.ServiceSyncer, error) {
	factory, err := lib.NewGoogleDriveSyncFactory(c.db)
	if err != nil {
		return nil, err
	}
	factory.Debug = c.debug
	console := &GoogleDriveSyncConsoleFactory{
		GoogleDriveSyncFactory: factory,
	}
	csc := lib.NewStorageController(c.db, cs)
	return console.Create(csc)
}

func (c *ConsoleController) ConnectGoogleDrive(id string, secret string) error {
	credentials := &lib.GoogleDriveApiCredentials{
		ClientID:     id,
		ClientSecret: secret,
	}
	if credentials.ClientID == "" {
		credentials.ClientID = DefaultGoogleClientId
	}
	if credentials.ClientSecret == "" {
		credentials.ClientSecret = DefaultGoogleClientSecret
	}
	cs := &lib.ConnectedStorage{StorageId: lib.StorageGoogleDrive}
	cs.WriteCredentials(credentials)
	_, err := c.NewGoogleDriveSync(cs)
	return err
}
func (c *ConsoleController) NewDummyStorage() (lib.ServiceSyncer, error) {
	credentials := &lib.DummySyncCredentials{}
	cs := &lib.ConnectedStorage{StorageId: lib.StorageDummy}
	cs.WriteCredentials(credentials)
	factory, err := lib.NewDummySyncFactory(c.db, c.debug)
	if err != nil {
		return nil, err
	}
	factory.Debug = c.debug
	csc := lib.NewStorageController(c.db, cs)
	csc.SaveCredentials(credentials)
	return factory.Create(csc)
}
func (c *ConsoleController) ConnectDummyStorage() error {
	_, err := c.NewDummyStorage()
	return err
}

type SyncController struct {
	sync         *lib.Sync
	showProgress bool
}

func (s *SyncController) Changes() ([]*lib.FileChange, error) {
	return s.sync.GetChanges()
}
func (s *SyncController) Start() error {
	for {
		err := s.Sync()
		if err != nil {
			return err
		}
		time.Sleep(time.Duration(5) * time.Second)
	}
	return nil
}
func (s *SyncController) Sync() error {
	changec := make(chan *lib.FileChange)
	progressc := make(chan *lib.Progress)
	hasChanges := false
	go func() {
		err := s.sync.Sync(changec, progressc)
		checkErr(err)
	}()
	var bar *pb.ProgressBar
	var start time.Time
Loop:
	for {
		select {
		case c := <-changec:
			if !hasChanges {
				start = time.Now()
				fmt.Println("Changes list:")
			}
			hasChanges = true
			printChange(c)
		case p, ok := <-progressc:
			if !ok {
				if bar != nil {
					bar.FinishPrint(fmt.Sprintf("Synced in %s\n", time.Since(start)))
					bar = nil
				}
				break Loop
			}
			if bar == nil && s.showProgress {
				bar = pb.StartNew(p.Total)
			}
			if bar != nil {
				bar.Set(p.Value)
			}
		}
	}
	return nil
}

func printChange(c *lib.FileChange) {
	target := "?"
	if c.Target != nil {
		target = c.Target.Name()
	}
	path := c.File.Path
	if c.Type() == lib.FileChangeMove {
		path = fmt.Sprintf("%s(old %s)", path, c.File.ParentVersionFile.Path)
	}
	fmt.Printf(" * %s %s (%s -> %s)\n", c.Type(), path, c.Source.Name(), target)
}

func console() *cli.App {
	app := cli.NewApp()
	app.Name = "console"
	app.Usage = "Synchronize with multiple storage services"
	app.Author = "Aidas Klimas"
	app.Version = "0.1"
	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:  "debug, d",
			Usage: "enable debug messages",
		},
		cli.StringFlag{
			Name:  "path, p",
			Value: "",
			Usage: "directory path to be synced",
		},
	}
	app.Commands = []cli.Command{
		{
			Name:  "add",
			Usage: "[storage provider] [Client Id] [Client Secret]",
			Action: func(context *cli.Context) {
				c := NewConsoleController(context)
				storage := &lib.Storage{}
				err := c.db.Where("name = ?", context.Args().First()).Find(&storage).Error
				if err != nil {
					fmt.Println("Record was not found")
					return
				}
				switch storage.Id {
				case lib.StorageGoogleDrive:
					err := c.ConnectGoogleDrive(context.Args().Get(1), context.Args().Get(2))
					checkErr(err)
				case lib.StorageDummy:
					err := c.ConnectDummyStorage()
					checkErr(err)
				}
				fmt.Println("Storage was added")
			},
		},
		{
			Name:  "list",
			Usage: "List connected storages",
			Action: func(context *cli.Context) {
				c := NewConsoleController(context)
				storages := []lib.ConnectedStorage{}
				err := c.db.Preload("Storage").Find(&storages).Error
				checkErr(err)
				fmt.Printf("Got %d storages\n", len(storages))
				for _, storage := range storages {
					fmt.Printf("#%d %s\n", storage.Id, storage.Storage.Name)
				}
			},
		},
		{
			Name:  "remove",
			Usage: "[id] Remove connected storage by id",
			Action: func(context *cli.Context) {
				c := NewConsoleController(context)
				storage := &lib.ConnectedStorage{}
				err := c.db.Where("id=?", context.Args().First()).First(storage).Error
				if err != nil {
					fmt.Println("Record was not found")
					return
				}
				csc := lib.NewStorageController(c.db, storage)
				err = csc.Delete()
				checkErr(err)
				fmt.Println("Storage was removed")
			},
		},
		{
			Name:  "run",
			Usage: "Start synchronization and listen for changes",
			Action: func(context *cli.Context) {
				var err error
				c := NewConsoleController(context)
				sync, err := lib.NewSync(c.db, c.folderSync, c.debug)
				checkErr(err)
				controller := &SyncController{sync: sync, showProgress: true}
				err = controller.Start()
				checkErr(err)
			},
		},
		{
			Name:  "sync",
			Usage: "Synchinize files once",
			Action: func(context *cli.Context) {
				var err error
				c := NewConsoleController(context)
				sync, err := lib.NewSync(c.db, c.folderSync, c.debug)
				checkErr(err)
				controller := &SyncController{sync: sync}
				err = controller.Sync()
				checkErr(err)
			},
		},
		{
			Name:  "changes",
			Usage: "Show changes list",
			Action: func(context *cli.Context) {
				var err error
				c := NewConsoleController(context)
				sync, err := lib.NewSync(c.db, c.folderSync, c.debug)
				checkErr(err)
				controller := &SyncController{sync: sync}
				changes, err := controller.Changes()
				checkErr(err)
				for _, c := range changes {
					printChange(c)
				}
			},
		},
	}
	return app
}
func main() {
	// path := "./test/"
	// os.Args = []string{"", "-p", path, "add", "Google Drive"}
	// os.Args = []string{"", "-p", path, "run"}
	// os.Args = []string{"", "-d", "-p", path, "list"}
	console().Run(os.Args)
}
