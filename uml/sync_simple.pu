@startuml
!include style.pu
/'
class FileChunk {
    +Id() string
    +FileId() string
    +Size() string
    +Offset() int
    +Meta(serviceId string) interface{}
}
class File {
    +Id() string
    +Path() string
    +Md5() string
    +Size() string
    +ModifiedAt() int
    +IsUploaded() bool
    +IsDownloaded() bool
    +Chunks() FileChunk[]
}
File-r-*FileChunk
interface Downloadable {
    +Download(path string) (chan int, error)
}
interface Uploadable {
    +Path() (string)
}
class FileDownloadable {
}
FileDownloadable-u-|>File
FileDownloadable..|>Downloadable
class FileUploadable {
}
FileUploadable-u-|>File
FileUploadable..|>Uploadable

class FileChange {
    +File() File
    +Type() int
}
FileChange-->File
'/

interface ServiceSyncer {
    +GetChanges() (FileChange[], error)
    +CreateFile(file File) (error)
    +UpdateFile(file File) (error)
    +RemoveFile(file File) (error)
    +MoveFile(file File) (error)
}
/'
interface ServiceSyncerFactory {
    +Create() (ServiceSyncer, error)
}
ServiceSyncerFactory-->ServiceSyncer
'/
class Sync {
    +SetMainStorage(service ServiceSyncer) error
    +AddStorage(service ServiceSyncer) error
    #GetChanges() (chan FileChange, error)
}
Sync-r-*ServiceSyncer

class SyncController {
    +Start() (chan FileChange, error)
    +Stop() (error)
    #Sync() (chan FileChange, error)
}
SyncController-u->Sync
/'
class FolderSync {
}

FolderSync--|>ServiceSyncer
abstract FolderSyncFactory {
    +Create() (FolderSync, error)
}
FolderSyncFactory-->FolderSync
FolderSyncFactory-u-|>ServiceSyncerFactory


class GoogleDriveSync {
}
GoogleDriveSync--|>ServiceSyncer
abstract GoogleDriveSyncFactory {
    +Create() (GoogleDriveSync, error)
}
GoogleDriveSyncFactory-->GoogleDriveSync
GoogleDriveSyncFactory-u-|>ServiceSyncerFactory

class KitiSync ##[dashed] {
}
KitiSync--|>ServiceSyncer
abstract KitiSyncFactory ##[dashed] {
    +Create() (KitiSync, error)
}
KitiSyncFactory-->KitiSync
KitiSyncFactory-u-|>ServiceSyncerFactory


note "Skirtingos implementacijos priklausomai nuo aplikacijos tipo:\ntelefonas, kompiuterio aplikacija ar tinklalapis. \nJei reikia, atliekamas prisijungimas prie serviso" as noteFactory
noteFactory .. FolderSyncFactory
noteFactory .. GoogleDriveSyncFactory
noteFactory .. KitiSyncFactory
'/
@enduml

