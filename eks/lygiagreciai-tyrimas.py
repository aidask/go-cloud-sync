#!/usr/bin/python -u
 
"""
Dropbox File Transfer Comparison - POC

This program compares the speed of uploading multiple files to dropbox using
both queued and parallel connections per file.
"""
 
import time
import threading
import dropbox
import sys
 
 
app_key = 'fdftzwseyoqxzx1'
app_secret = 'mffquu98zosmk45'
 
 
class UploadThread(threading.Thread):
    def __init__(self, client, file, n):
        threading.Thread.__init__(self)
        self.client = client
        self.file = file
        self.n = n
        self.retry = False
 
    def run(self):
        while True:
            f = open(self.file, 'rb')
            try:
                response = self.client.put_file('/tmp/p%s.jpg' % self.n, f)
                self.retry=False
                break
            except Exception as e:
                print 'EXCEPTION %s' % e
                self.retry=True
 
def create_file(i, fileSize):
    with open("files/" + str(i), "wb") as out:
            out.truncate(fileSize)
    return "files/" + str(i)
 
def upload_in_sequence(client, files):
    start_time = time.time()
    for (i, file) in enumerate(files):
        f = open(file, 'rb')
        response = client.put_file('/tmp/' + str(i), f)
 
    time_taken = time.time() - start_time
    print 'in sequence time taken (seconds): %s' % round(time_taken, 5)
 
 
def upload_in_parallel(client, files):
    retry = True 
    start_time = 0 
    while retry:
        start_time = time.time()
        retry = False
        threads = []
        for (i, file) in enumerate(files):
            threads.append(UploadThread(client, file, i))
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()
            if thread.retry:
                retry = True
                print 'RETRY'
 
    time_taken = time.time() - start_time
    print 'in parallel time taken (seconds): %s' % round(time_taken, 5)
 
 
if __name__ == "__main__":
    flow = dropbox.client.DropboxOAuth2FlowNoRedirect(app_key, app_secret)
 
    authorize_url = flow.start()
    if len(sys.argv) == 2:
        code = sys.argv[1]
    else:
        print '1. Go to: ' + authorize_url
        print '2. Click "Allow" (you might have to log in first)'
        print '3. Copy the authorization code.'
        code = raw_input("Enter the authorization code here: ").strip()
 
    try:
        access_token, user_id = flow.finish(code)
 
        client = dropbox.client.DropboxClient(access_token)
        print 'linked account: ', client.account_info()['display_name']
 
        fileCounts = [2,4,8,16,32,64,128]
        sizes = [1024]
        for size in sizes:
            for count in fileCounts:
                print 'Uploading ' + str(count) +  ' files of size ' + str(size)
                files = []
                for i in range(count):
                    files.append(create_file(i, size))
                upload_in_sequence(client, files)
                time.sleep(1)
                upload_in_parallel(client, files)
                time.sleep(1)

    except dropbox.rest.ErrorResponse:
        print 'invalid authorization code.'
 