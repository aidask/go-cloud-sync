package lib

import "testing"

func TestAddStorage(t *testing.T) {
	s := Sync{}
	storage := newStorage()
	s.AddStorage(storage)
	if len(s.storages) != 1 {
		t.Error("Storage was not added")
	}
}
func TestAnalyseChangesConflictCreate(t *testing.T) {
	s := initSync()
	fc1 := fileChange(FileChangeCreate, "file1", s.mainStorage)
	fc2 := fileChange(FileChangeCreate, "file1", s.storages[0])
	changes := []*FileChange{fc1, fc2}
	changes, _ = s.AnaliseChanges(changes)
	if len(changes) != 1 {
		t.Error("Unresolved conflict, only one file should be created")
	}
}
func TestAnalyseChangesConflictUpdate(t *testing.T) {
	s := initSync()
	fc1 := fileChange(FileChangeUpdate, "file1", s.mainStorage)
	fc2 := fileChange(FileChangeUpdate, "file1", s.storages[0])
	changes := []*FileChange{fc1, fc2}
	changes, _ = s.AnaliseChanges(changes)
	if len(changes) != 1 {
		t.Error("Unresolved conflict, only one file should be updated")
	}
}
func TestAnalyseChangesConflictRemove(t *testing.T) {
	s := initSync()
	fc1 := fileChange(FileChangeRemove, "file1", s.mainStorage)
	fc2 := fileChange(FileChangeRemove, "file1", s.storages[0])
	changes := []*FileChange{fc1, fc2}
	changes, _ = s.AnaliseChanges(changes)
	if len(changes) != 1 {
		t.Error("Unresolved conflict, only one file should be removed")
	}
}
func TestAnalyseChangesConflictUpdateRemove(t *testing.T) {
	s := initSync()
	fc1 := fileChange(FileChangeUpdate, "file1", s.mainStorage)
	fc2 := fileChange(FileChangeRemove, "file1", s.storages[0])
	changes := []*FileChange{fc1, fc2}
	changes, _ = s.AnaliseChanges(changes)
	if len(changes) != 1 && changes[0].Type() == FileChangeCreate {
		t.Error("Unresolved conflict, file should be created")
	}
}
func fileChange(changeType string, path string, source ServiceSyncer) *FileChange {
	return &FileChange{changeType: changeType, File: &File{Path: path}, Source: source}
}
func initSync() *Sync {
	s := &Sync{}
	stm := newStorage()
	st1 := newStorage()
	s.SetMainStorage(stm)
	s.AddStorage(st1)
	return s
}
func newStorage() ServiceSyncer {
	return &DummySync{}
}
