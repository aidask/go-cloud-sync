package lib

import "testing"

func TestPathFromMapSelf(t *testing.T) {
	m := map[string]string{}
	path, _ := pathFromMap(m, "", "hello")
	if path != "hello" {
		t.Errorf("invalid path %s", path)
	}
}
func TestPathFromMapParent(t *testing.T) {
	m := map[string]string{}
	m["a"] = "AFolder"
	path, _ := pathFromMap(m, "a", "hello")
	if path != "AFolder/hello" {
		t.Errorf("invalid path %s", path)
	}
}
