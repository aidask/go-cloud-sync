package lib

import (
	"time"

	"github.com/jinzhu/gorm"
)

type DummySyncCredentials struct {
}
type DummySyncFactory struct {
	*BaseFactory
}

func NewDummySyncFactory(db *gorm.DB, debug bool) (*DummySyncFactory, error) {
	d := &DummySyncFactory{
		BaseFactory: &BaseFactory{db: db, Debug: debug},
	}
	return d, nil
}
func (f *DummySyncFactory) Create(csc *ConnectedStorageController) (ServiceSyncer, error) {
	sync := &DummySync{
		BaseSync: &BaseSync{db: f.db, csc: csc, debug: f.Debug},
	}
	return sync, nil
}

type DummySync struct {
	*BaseSync
}

func (s *DummySync) Changes() ([]*FileChange, error) {
	changes := []*FileChange{}
	return changes, nil
}
func (s *DummySync) Create(c *FileChange) (e error) {
	chunk := &FileChunk{
		ConnectedStorageId: s.ConnectedStorageId(),
		Size:               c.File.Size,
		CustomFileId:       "",
		ModTime:            time.Now(),
	}
	c.File.FileChunks = append(c.File.FileChunks, chunk)
	s.SaveFile(c.File)
	return nil
}
func (s *DummySync) Remove(fc *FileChange) error {
	return nil
}
func (s *DummySync) Update(c *FileChange) error {
	return s.Create(c)
}
func (s *DummySync) Destroy() error {
	return nil
}
