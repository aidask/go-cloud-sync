package lib

import (
	"fmt"
	"path/filepath"
	"strings"
)

func NormalizePath(p string) string {
	abs, err := filepath.Abs(strings.Replace(p, "\\", "/", -1))
	if err != nil {
		panic(err)
	}
	return abs
}
func NormalizeDirPath(p string) string {
	return strings.TrimRight(NormalizePath(p), "/") + "/"
}
func pathFromMap(m map[string]string, parent, title string) (string, error) {
	for {
		name, ok := m[parent]
		if !ok {
			break
		}
		title = fmt.Sprintf("%s/%s", name, title)
		parent = name
	}
	return title, nil
}
