package lib

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

type FolderSyncCredentials struct {
	Path string
}
type FolderSyncFactory struct {
	*BaseFactory
}

func NewFolderSyncFactory(db *gorm.DB, debug bool) (*FolderSyncFactory, error) {
	f := &FolderSyncFactory{
		BaseFactory: &BaseFactory{db: db, Debug: debug},
	}
	return f, nil
}

func (f *FolderSyncFactory) Create(csc *ConnectedStorageController) (ServiceSyncer, error) {
	cr := &FolderSyncCredentials{}
	csc.Credentials(cr)
	s, err := os.Stat(cr.Path)
	if err != nil {
		return nil, err
	}
	if !s.IsDir() {
		return nil, errors.New("Synchronization path is not a directory")
	}
	sync := &FolderSync{
		BaseSync: &BaseSync{db: f.db, csc: csc, debug: f.Debug},
		path:     cr.Path,
	}
	return sync, nil
}

type FolderSync struct {
	*BaseSync
	path string
}

func (s *FolderSync) Changes() ([]*FileChange, error) {
	changes := []*FileChange{}
	err := filepath.Walk(s.path, func(path string, f os.FileInfo, err error) error {
		if f.IsDir() {
			return nil
		}
		path = s.relativePath(NormalizePath(path))
		if s.isIgnoredPath(path) {
			return nil
		}
		changeType := FileChangeUpdate
		file, err := s.FindFile(path)
		if err == gorm.RecordNotFound {
			changeType = FileChangeCreate
		} else if err != nil {
			panic(err)
		}
		changed := file.Size != f.Size() || f.ModTime().Unix() != file.ModTime.Unix()
		if changed {
			newFile := &File{
				Path:    path,
				Size:    f.Size(),
				ModTime: f.ModTime(),
			}
			if changeType != FileChangeCreate {
				newFile.ParentVersionFile = file
				newFile.ParentVersionFileId.Scan(file.Id)
			}
			changes = append(changes, &FileChange{File: newFile, changeType: changeType, Source: s})
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	files, err := s.FileList()
	if err != nil {
		return nil, err
	}
	for i, file := range files {
		_, err = os.Open(s.absolutePath(file.Path))
		if err != nil {
			changes = append(changes, &FileChange{File: &files[i], changeType: FileChangeRemove, Source: s})
		}
	}
	return changes, nil
}

type SyncDownloader interface {
	Download(*FileChunk, *os.File) error
}

func (f *FolderSync) Create(c *FileChange) (e error) {
	downloader, ok := c.Source.(SyncDownloader)
	if !ok {
		return errors.New("Source must have download method")
	}
	path := f.absolutePath(c.File.Path)
	dirPath := filepath.Dir(path)
	err := os.MkdirAll(dirPath, 0777)
	if err != nil {
		return err
	}
	fh, err := os.Create(path)
	if err != nil {
		return err
	}
	save := func() error {
		currenttime := time.Now().Local()
		err = os.Chtimes(path, currenttime, currenttime)

		if err != nil {
			return err
		}
		stat, e := os.Stat(path)
		if e != nil {
			return err
		}
		c.File.Size = stat.Size()
		c.File.ModTime = currenttime
		return f.SaveFile(c.File)
	}
	defer func() {
		fh.Close()
		if e == nil {
			e = save()
		}
		if e != nil {
			err := os.Remove(path)
			if err != nil {
				fmt.Println("Error:", err)
			}
			return
		}
	}()
	for i := range c.File.FileChunks {
		err = downloader.Download(c.File.FileChunks[i], fh)
		if err != nil {
			return err
		}
	}

	return nil
}
func (f *FolderSync) Remove(fc *FileChange) error {
	if fc.File.Path == "" {
		fmt.Println(fc.File)
		panic("File is not initialized")
	}
	base := f.absolutePath(fc.File.Path)
	for {
		err := os.Remove(base)
		if err != nil {
			break
		}
		base = filepath.Dir(base)
	}
	return f.DeleteFile(fc.File)
}
func (f *FolderSync) Update(c *FileChange) error {
	if c.File.ParentVersionFile.Path != c.File.Path {
		err := os.Rename(f.absolutePath(c.File.ParentVersionFile.Path), f.absolutePath(c.File.Path))
		if err != nil {
			return err
		}
	}
	return f.Create(c)
}
func (f *FolderSync) Move(c *FileChange) error {
	err := os.Rename(f.absolutePath(c.File.ParentVersionFile.Path), f.absolutePath(c.File.Path))
	if err != nil {
		return err
	}
	return f.SaveFile(c.File)
}
func (f *FolderSync) Destroy() error {
	return nil
}
func (f *FolderSync) relativePath(path string) string {
	return strings.TrimLeft(path[len(f.path):len(path)], "/")
}
func (f *FolderSync) absolutePath(path string) string {
	return f.path + path
}
func (s *FolderSync) ResolveConflict(f *File) error {
	filePath := ""
	for i := 0; i < 99; i++ {
		pref := ""
		if i != 0 {
			pref = fmt.Sprintf(".%d", i)
		}
		filePath = s.absolutePath(f.Path + pref + ConflictFileExtension)
		_, err := os.Open(filePath)
		if err != nil {
			break
		}
	}
	return cp(s.absolutePath(f.Path), filePath)
}
