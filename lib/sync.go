package lib

import (
	"errors"
	"fmt"

	"github.com/jinzhu/gorm"
)

type Sync struct {
	mainStorage ServiceSyncer
	storages    []ServiceSyncer
	nextStorage int
}

func NewSync(db *gorm.DB, mainStorage ServiceSyncer, debug bool) (*Sync, error) {
	sync := &Sync{}
	sync.SetMainStorage(mainStorage)
	storages := []ConnectedStorage{}
	err := db.Preload("Storage").Find(&storages).Error
	if err != nil {
		return nil, err
	}
	for i, storage := range storages {
		var s ServiceSyncer
		switch storage.Storage.Id {
		case StorageGoogleDrive:
			factory, err := NewGoogleDriveSyncFactory(db)
			if err != nil {
				return nil, err
			}
			factory.Debug = debug
			csc := NewStorageController(db, &storages[i])
			s, err = factory.Create(csc)
			if err != nil {
				return nil, err
			}
		case StorageDummy:
			factory, err := NewDummySyncFactory(db, debug)
			if err != nil {
				return nil, err
			}
			csc := NewStorageController(db, &storages[i])
			s, err = factory.Create(csc)
			if err != nil {
				return nil, err
			}
		case StorageFolder:
			continue
		default:
			panic("Invalid storage id")
		}
		sync.AddStorage(s)
	}
	return sync, nil
}

func (s *Sync) SetMainStorage(ss ServiceSyncer) {
	s.mainStorage = ss
}
func (s *Sync) Storages() []ServiceSyncer {
	return s.storages
}
func (s *Sync) AddStorage(ss ServiceSyncer) {
	s.storages = append(s.storages, ss)
}
func (s *Sync) RemoveStorageById(id int) {
	for i, stor := range s.storages {
		if stor.Id() == id {
			s.storages = append(s.storages[:i], s.storages[i+1:]...)
		}
	}
}
func (s *Sync) GetStorageById(id int) ServiceSyncer {
	for i, stor := range s.storages {
		if stor.Id() == id {
			return s.storages[i]
		}
	}
	return nil
}

func (s *Sync) GetChanges() ([]*FileChange, error) {
	fc := []*FileChange{}
	changes, err := s.mainStorage.Changes()
	if err != nil {
		return nil, err
	}
	fc = append(fc, changes...)
	for _, storage := range s.storages {
		changes, err := storage.Changes()
		if err != nil {
			return nil, err
		}
		fc = append(fc, changes...)
	}
	return fc, nil
}

/**
* Possible conflicts:
* create - create rename one file
* rename local file to .conflict
* update - update rename one file and
* rename local file to .conflict
* update - remove create new file
* remove remove change
* remove - remove leave one, but should not trigger an err
* name conflict
 */
func (s *Sync) AnaliseChanges(changes []*FileChange) ([]*FileChange, error) {
	pathmap := map[string]*FileChange{}
	c := []*FileChange{}
	for i := range changes {
		fc := changes[i]
		if pathmap[fc.File.Path] != nil {
			fc2 := pathmap[fc.File.Path]
			switch {
			case fc.Type() == FileChangeUpdate && fc2.Type() == FileChangeUpdate:
				fallthrough
			case fc.Type() == FileChangeCreate && fc2.Type() == FileChangeCreate:
				s.mainStorage.ResolveConflict(fc.File)
				if fc2.Source == s.mainStorage {
					sliceReplace(c, fc2, fc)
					fc.AddConflict(fc2)
				} else {
					fc2.AddConflict(fc)
				}
			case fc.Type() == FileChangeUpdate && fc2.Type() == FileChangeRemove:
				err := s.updateRemoveConflict(c, fc2, fc)
				if err != nil {
					return nil, err
				}
			case fc.Type() == FileChangeRemove && fc2.Type() == FileChangeUpdate:
				err := s.updateRemoveConflict(c, fc, fc2)
				if err != nil {
					return nil, err
				}
			case fc.Type() == FileChangeRemove && fc2.Type() == FileChangeRemove:
				fc2.suppressErrors = true
				fc2.AddConflict(fc)
			default:
				fmt.Printf("Unhandled conflict %s - %s", fc.Type(), fc2.Type())
			}
			continue
		}
		pathmap[fc.File.Path] = fc
		c = append(c, fc)
	}
	return c, nil
}
func (s *Sync) updateRemoveConflict(c []*FileChange, fcRemove, fcUpdate *FileChange) error {
	fcUpdate.changeType = FileChangeCreate
	sliceReplace(c, fcRemove, fcUpdate)
	fcUpdate.AddConflict(fcRemove)
	return nil
}

func (s *Sync) Sync(changec chan *FileChange, progressc chan *Progress) error {
	defer close(progressc)
	if len(s.storages) < 1 {
		return errors.New("Could not start sync without connected storages")
	}
	changes, err := s.GetChanges()
	if err != nil {
		return err
	}
	changes, err = s.AnaliseChanges(changes)
	if err != nil {
		return err
	}
	if len(changes) == 0 {
		return nil
	}
	p := &Progress{Total: len(changes)}
	for i, c := range changes {
		var target ServiceSyncer
		if c.Source == s.mainStorage {
			if c.Type() == FileChangeCreate {
				target = s.NextStorage()
			} else {
				target = s.GetStorageById(c.defaultTarget())
			}
		} else {
			target = s.mainStorage
		}
		changes[i].Target = target
		changec <- changes[i]
	}
	progressc <- p
	for i := range changes {
		err = changes[i].Apply()
		if err != nil {
			return err
		}
		p.Value += 1
		progressc <- p
	}
	return nil
}
func (s *Sync) NextStorage() ServiceSyncer {
	if s.nextStorage >= len(s.storages) {
		s.nextStorage = 0
	}
	st := s.storages[s.nextStorage]
	s.nextStorage += 1
	return st
}
