package lib

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"reflect"
	"strings"

	"github.com/jinzhu/gorm"
)

const (
	FileChangeCreate = "create"
	FileChangeUpdate = "update"
	FileChangeRemove = "remove"
	FileChangeMove   = "move"

	DefaultBaseFolderName = "Sync"

	ConflictFileExtension = ".conflict"
)

type Progress struct {
	Total int
	Value int
}

func (p *Progress) Percent() float64 {
	return float64(p.Value) / float64(p.Total)
}

type FileChange struct {
	File           *File
	Source         ServiceSyncer
	Target         ServiceSyncer
	changeType     string
	suppressErrors bool
	Conflicts      []*FileChange
}

func (f *FileChange) Type() string {
	return f.changeType
}

func (f *FileChange) Apply() error {
	var err error
	switch f.Type() {
	case FileChangeCreate:
		err = f.Target.Create(f)
	case FileChangeRemove:
		err = f.Target.Remove(f)
	case FileChangeUpdate:
		err = f.Target.Update(f)
	default:
		err = f.Target.Move(f)
	}
	if f.suppressErrors {
		return nil
	}
	return err
}

func (f *FileChange) AddConflict(fc *FileChange) {
	f.Conflicts = append(f.Conflicts, fc)
}
func (f *FileChange) defaultTarget() int {
	if f.File.ParentVersionFile != nil {
		return f.File.ParentVersionFile.FileChunks[0].ConnectedStorageId
	} else {
		return f.File.FileChunks[0].ConnectedStorageId
	}
}

type ServiceSyncer interface {
	Id() int
	Name() string
	Changes() ([]*FileChange, error)
	Create(file *FileChange) error
	Update(file *FileChange) error
	Remove(File *FileChange) error
	Move(File *FileChange) error
	ResolveConflict(f *File) error
	Destroy() error
}
type ServiceSyncerFactory interface {
	Create(*ConnectedStorageController) (ServiceSyncer, error)
}

type BaseFactory struct {
	db    *gorm.DB
	Debug bool
}

type BaseSync struct {
	db    *gorm.DB
	csc   *ConnectedStorageController
	debug bool
}

func (b *BaseSync) Id() int {
	return b.csc.cs.Id
}
func (b *BaseSync) Name() string {
	return fmt.Sprintf("#%d %s", b.csc.cs.Id, b.csc.cs.Storage.Name)
}
func (b *BaseSync) Destroy() error {
	return b.csc.Delete()
}
func (b *BaseSync) Move(fc *FileChange) error {
	return nil
}
func (b *BaseSync) ConnectedStorageId() int {
	return b.csc.cs.Id
}
func (b *BaseSync) FolderSource(s ServiceSyncer) (*FolderSync, error) {
	f, ok := s.(*FolderSync)
	if !ok {
		return nil, fmt.Errorf("Service %s can not accept changes from %s", b.Name(), s.Name())
	}
	return f, nil
}
func (b *BaseSync) FindChunk(customId string) (*FileChunk, error) {
	fc := &FileChunk{}
	err := b.db.Where(FileChunk{
		ConnectedStorageId: b.ConnectedStorageId(),
		CustomFileId:       customId,
	}).Where("(select f.id from files f where f.parent_version_file_id=file_id limit 1) IS NULL").Preload("File").First(fc).Error
	return fc, err
}
func (b *BaseSync) SaveChunk(chunk *FileChunk) error {
	return b.db.Save(chunk).Error
}
func (b *BaseSync) FindFile(path string) (*File, error) {
	file := &File{}
	err := b.db.Where("(SELECT pf.id FROM files pf WHERE files.id=pf.parent_version_file_id limit 1) IS NULL AND path=?", path).Preload("FileChunks").First(file).Error
	return file, err
}
func (b *BaseSync) FileList() ([]File, error) {
	files := []File{}
	err := b.db.Preload("FileChunks").Where("(SELECT pf.id FROM files pf WHERE files.id=pf.parent_version_file_id LIMIT 1) IS NULL").Find(&files).Error
	return files, err
}
func (b *BaseSync) SaveFile(file *File) error {
	return b.db.Save(file).Error
}
func (b *BaseSync) DeleteFile(file *File) error {
	err := b.db.Where("file_id in (select id from files where path=?)", file.Path).Delete(FileChunk{}).Error
	if err != nil {
		return err
	}
	return b.db.Where("path=?", file.Path).Delete(File{}).Error
}
func (s *BaseSync) ResolveConflict(f *File) error {
	return nil
}
func (b *BaseSync) isIgnoredPath(path string) bool {
	return len(path) == 0 || path[0] == '.' || strings.Index(path, "/.") > -1 || path[len(path)-1] == '~' || filepath.Ext(path) == ConflictFileExtension
}

func cp(src, dst string) error {
	s, err := os.Open(src)
	if err != nil {
		return err
	}
	defer s.Close()
	d, err := os.Create(dst)
	if err != nil {
		return err
	}
	if _, err := io.Copy(d, s); err != nil {
		d.Close()
		return err
	}
	return d.Close()
}
func sliceReplace(slice interface{}, find interface{}, replace interface{}) int {
	s := reflect.ValueOf(slice)
	if s.Kind() != reflect.Slice {
		panic("InterfaceSlice() given a non-slice type")
	}
	re := reflect.ValueOf(replace)
	fe := reflect.ValueOf(find)
	r := 0
	for i := 0; i < s.Len(); i++ {
		if s.Index(i).Pointer() == fe.Pointer() {
			s.Index(i).Set(re)
			r += 1
		}
	}
	return r
}
