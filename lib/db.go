package lib

import (
	"database/sql"
	"encoding/json"
	"time"

	"github.com/jinzhu/gorm"
)

const (
	StorageFolder      int = 3
	StorageDummy       int = 4
	StorageDropbox     int = 1
	StorageGoogleDrive int = 2
)

type File struct {
	Id                  int
	Path                string
	Size                int64 `sql:"type:bigint"`
	ParentVersionFile   *File
	ParentVersionFileId sql.NullInt64
	Status              int
	ModTime             time.Time
	CreatedAt           time.Time `sql:"DEFAULT:current_timestamp"`
	FileChunks          []*FileChunk
}
type FileChunk struct {
	Id                 int
	File               File
	FileId             int
	ConnectedStorage   ConnectedStorage
	ConnectedStorageId int
	CustomFileId       string
	Size               int64 `sql:"type:bigint"`
	Offset             int64 `sql:"type:bigint"`
	ModTime            time.Time
	CreatedAt          time.Time   `sql:"DEFAULT:current_timestamp"`
	Data               interface{} `sql:"-"`
}
type Storage struct {
	Id        int
	Name      string    `sql:"unique"`
	CreatedAt time.Time `sql:"DEFAULT:current_timestamp"`
}
type ConnectedStorage struct {
	Id          int
	Storage     Storage
	StorageId   int
	Credentials string `sql:"type:text"`
}

func (b *ConnectedStorage) ReadCredentials(cr interface{}) error {
	return json.Unmarshal([]byte(b.Credentials), cr)
}
func (g *ConnectedStorage) WriteCredentials(cr interface{}) error {
	b, err := json.Marshal(cr)
	if err != nil {
		return err
	}
	g.Credentials = string(b)
	return nil
}

type ConnectedStorageController struct {
	db *gorm.DB
	cs *ConnectedStorage
}

func NewStorageController(db *gorm.DB, cs *ConnectedStorage) *ConnectedStorageController {
	return &ConnectedStorageController{db, cs}
}

func (c *ConnectedStorageController) Save() error {
	if c.db.NewRecord(c.cs.Storage) {
		c.db.Model(&c.cs).Related(&c.cs.Storage)
	}
	return c.db.Save(c.cs).Error
}
func (c *ConnectedStorageController) Delete() error {
	err := c.db.Where("connected_storage_id=?", c.cs.Id).Delete(FileChunk{}).Error
	if err != nil {
		return err
	}
	err = c.db.Where("(select c.id from file_chunks c where c.connected_storage_id=? and c.file_id=files.id) is null", c.cs.Id).Delete(File{}).Error
	if err != nil {
		return err
	}
	return c.db.Delete(c.cs).Error
}
func (c *ConnectedStorageController) Credentials(cr interface{}) error {
	return c.cs.ReadCredentials(cr)
}
func (c *ConnectedStorageController) SetCredentials(cr interface{}) error {
	return c.cs.WriteCredentials(cr)
}
func (c *ConnectedStorageController) SaveCredentials(cr interface{}) error {
	err := c.SetCredentials(cr)
	if err != nil {
		return err
	}
	return c.Save()
}

func InitDb(db *gorm.DB) error {
	db.AutoMigrate(new(Storage))
	db.AutoMigrate(new(ConnectedStorage))
	db.AutoMigrate(new(File))
	db.AutoMigrate(new(FileChunk))
	db.Model(&File{}).AddIndex("idx_path", "path")
	db.Model(&FileChunk{}).RemoveIndex("idx_chunk_id_storageid")
	db.Model(&FileChunk{}).AddIndex("idx_chunk_id_storageid", "connected_storage_id", "custom_file_id")

	storage := Storage{Id: StorageFolder, Name: "Folder"}
	db.Create(&storage)
	storage = Storage{Id: StorageGoogleDrive, Name: "Google Drive"}
	db.Create(&storage)
	storage = Storage{Id: StorageDropbox, Name: "DropBox"}
	db.Create(&storage)
	storage = Storage{Id: StorageDummy, Name: "Dummy"}
	db.Create(&storage)
	return nil
}
