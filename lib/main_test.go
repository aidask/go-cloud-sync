package lib

import "testing"

func TestSliceReplace(t *testing.T) {
	ref1 := 1
	ref2 := 1
	s := []*int{&ref1, &ref2, &ref1}
	r := sliceReplace(s, &ref2, &ref1)
	if s[0] != &ref1 {
		t.Error("Wrong element replaced")
	}
	if s[1] != &ref1 {
		t.Error("Wrong element replaced")
	}
	if s[2] != &ref1 {
		t.Error("Wrong element replaced")
	}
	if r != 1 {
		t.Error("Invalid count", r)
	}
}

func TestProgressPercent(t *testing.T) {
	p := Progress{Total: 10, Value: 5}
	if p.Percent() != 0.5 {
		t.Error("Invalid progress", p.Percent())
	}
}
