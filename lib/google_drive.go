package lib

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
	"golang.org/x/oauth2"
	"google.golang.org/api/drive/v2"
)

const GoogleDriveMimeTypeFolder = "application/vnd.google-apps.folder"

type GoogleDriveApiCredentials struct {
	ClientID       string `json:client_id`
	ClientSecret   string `json:client_secret`
	Token          *oauth2.Token
	BaseFolderId   string
	BaseFolderName string
}

func (c *GoogleDriveApiCredentials) IsAuthenticated() bool {
	return c.Token != nil
}

type GoogleDriveSyncFactory struct {
	*BaseFactory
}

func NewGoogleDriveSyncFactory(db *gorm.DB) (*GoogleDriveSyncFactory, error) {
	f := &GoogleDriveSyncFactory{
		BaseFactory: &BaseFactory{db: db},
	}
	return f, nil
}
func (g *GoogleDriveSyncFactory) CreateOauth2Config(cr *GoogleDriveApiCredentials) *oauth2.Config {
	return &oauth2.Config{
		ClientID:     cr.ClientID,
		ClientSecret: cr.ClientSecret,
		Scopes:       []string{"https://www.googleapis.com/auth/drive"},
		RedirectURL:  "urn:ietf:wg:oauth:2.0:oob",
		Endpoint: oauth2.Endpoint{
			AuthURL:  "https://accounts.google.com/o/oauth2/auth",
			TokenURL: "https://accounts.google.com/o/oauth2/token",
		},
	}
}

func (g *GoogleDriveSyncFactory) Create(csc *ConnectedStorageController) (ServiceSyncer, error) {
	cr := &GoogleDriveApiCredentials{}
	err := csc.Credentials(cr)
	if err != nil {
		return nil, err
	}
	if cr.Token == nil {
		return nil, errors.New("GoogleDrive service is not authorized")
	}
	config := g.CreateOauth2Config(cr)
	httpClient := config.Client(oauth2.NoContext, cr.Token)
	if g.Debug {
		httpClient.Transport = &logTransport{rt: httpClient.Transport}
	}
	svc, err := drive.New(httpClient)
	if err != nil {
		log.Fatalf("An error occurred creating Drive client: %v\n", err)
	}
	sync := &GoogleDriveSync{
		BaseSync: &BaseSync{db: g.db, csc: csc, debug: g.Debug},
		client:   svc,
		http:     httpClient,
	}
	if cr.BaseFolderId == "" {
		if cr.BaseFolderName == "" {
			cr.BaseFolderName = DefaultBaseFolderName
		}
	} else {
		f, err := svc.Files.Get(cr.BaseFolderId).Do()
		if err != nil {
			cr.BaseFolderId = ""
			fmt.Printf("Sync folder does not exists (Response: `%s`).\nCreating new.\n", err)
		} else if f.Labels.Trashed {
			return nil, errors.New("Sync folder is trashed.")
		}
	}
	if cr.BaseFolderId == "" {
		cr.BaseFolderId, err = sync.initFolder()
		if err != nil {
			return nil, err
		}
		err = csc.SaveCredentials(cr)
		if err != nil {
			return nil, err
		}
	}
	return sync, nil
}

type GoogleDriveSync struct {
	*BaseSync
	chunks bool
	client *drive.Service
	http   *http.Client
}

func (a *GoogleDriveSync) credentials() *GoogleDriveApiCredentials {
	cr := &GoogleDriveApiCredentials{}
	err := a.csc.Credentials(cr)
	if err != nil {
		panic(err)
	}
	return cr
}

func (a *GoogleDriveSync) baseFolderId() string {
	return a.credentials().BaseFolderId
}

func (a *GoogleDriveSync) initFolder() (string, error) {
	f := &drive.File{
		Title:       a.credentials().BaseFolderName,
		Description: "Base folder for sync service",
		MimeType:    GoogleDriveMimeTypeFolder,
	}
	r, err := a.client.Files.Insert(f).Do()
	if err != nil {
		return "", fmt.Errorf("An error occurred while initializing main dir: %v\n", err)
	}
	return r.Id, nil
}

func (a *GoogleDriveSync) Name() string {
	return fmt.Sprintf("%s(%s)", a.BaseSync.Name(), a.credentials().BaseFolderName)
}

func (a *GoogleDriveSync) Download(fc *FileChunk, f *os.File) error {
	url, ok := fc.Data.(string)
	if !ok {
		return errors.New("Can not download chunk, invalid data")
	}
	response, err := a.http.Get(url)
	if err != nil {
		return fmt.Errorf("Error while downloading %s - %s", url, err)
	}
	if response.StatusCode != 200 {
		return fmt.Errorf("Invalid download status code %s - %s", url, response.Status)
	}
	defer response.Body.Close()
	_, err = io.Copy(f, response.Body)
	if err != nil {
		return fmt.Errorf("Error to copy %s", err)
	}
	return nil
}

// https://developers.google.com/drive/v2/reference/files
func (a *GoogleDriveSync) Changes() ([]*FileChange, error) {
	var chunk *FileChunk
	folders := map[string]string{}
	chunks := map[int]*FileChunk{}
	changes := []*FileChange{}
	pageToken := ""
	inFolders := []string{a.baseFolderId()}
	search := []string{}
	for len(inFolders) > 0 {
		search = nil
		for _, id := range inFolders {
			search = append(search, fmt.Sprintf("'%s' in parents", id))
		}
		inFolders = nil
		q := a.client.Files.List()
		q.Q(strings.Join(search, " or "))
		for {
			if pageToken != "" {
				q = q.PageToken(pageToken)
			}
			r, err := q.Do()
			if err != nil {
				fmt.Printf("An error occurred: %v\n", err)
				return changes, err
			}
			if a.debug {
				fmt.Printf("Recieved %d files in response\n", len(r.Items))
			}
			for _, file := range r.Items {
				if file.Labels.Trashed {
					continue
				}
				if file.MimeType == GoogleDriveMimeTypeFolder {
					if path, ok := folders[file.Id]; ok {
						folders[file.Id] = fmt.Sprintf("%s/%s", path, file.Title)
					} else {
						folders[file.Id] = file.Title
					}
					inFolders = append(inFolders, file.Id)
					continue
				}
				t, err := time.Parse(time.RFC3339, file.ModifiedDate)
				if err != nil {
					return nil, err
				}
				chunk = &FileChunk{
					CustomFileId:       file.Id,
					Size:               file.FileSize,
					ConnectedStorageId: a.ConnectedStorageId(),
					ModTime:            t,
					Data:               file.DownloadUrl,
				}
				f := &File{
					Path:    file.Title,
					Size:    file.FileSize,
					ModTime: t,
				}
				if path, ok := folders[file.Parents[0].Id]; ok {
					f.Path = fmt.Sprintf("%s/%s", path, file.Title)
				}
				f.FileChunks = append(f.FileChunks, chunk)
				oldChunk, err := a.FindChunk(file.Id)
				if err == gorm.RecordNotFound {
					changes = append(changes, &FileChange{changeType: FileChangeCreate, File: f, Source: a})
				} else if err != nil {
					return nil, err
				} else if oldChunk.Size != file.FileSize || oldChunk.ModTime.Unix() != t.Unix() {
					f.ParentVersionFile = &oldChunk.File
					f.ParentVersionFileId.Scan(oldChunk.FileId)
					changes = append(changes, &FileChange{changeType: FileChangeUpdate, File: f, Source: a})
					chunks[oldChunk.Id] = chunk
				} else if oldChunk.File.Path != f.Path {
					f.ParentVersionFile = &oldChunk.File
					f.ParentVersionFileId.Scan(oldChunk.FileId)
					changes = append(changes, &FileChange{changeType: FileChangeMove, File: f, Source: a})
					chunks[oldChunk.Id] = chunk
				} else {
					chunks[oldChunk.Id] = chunk
				}
			}
			pageToken = r.NextPageToken
			if pageToken == "" {
				break
			}
		}
	}
	dbfiles, err := a.FileList()
	if err != nil {
		return nil, err
	}
	for i, file := range dbfiles {
		for _, c := range file.FileChunks {
			if _, ok := chunks[c.Id]; c.ConnectedStorageId == a.Id() && !ok {
				changes = append(changes, &FileChange{File: &dbfiles[i], changeType: FileChangeRemove, Source: a})
			}
		}
	}
	return changes, nil
}
func (a *GoogleDriveSync) Create(c *FileChange) error {
	f := &drive.File{}
	return a.upload(c, f)
}
func (a *GoogleDriveSync) Update(c *FileChange) error {
	if len(c.File.ParentVersionFile.FileChunks) != 1 {
		panic("Only one chunk can be updated")
	}
	f := &drive.File{}
	return a.upload(c, f)
}
func (a *GoogleDriveSync) parentOfPath(path string) (*drive.ParentReference, error) {
	list := filepath.SplitList(path)
	baseId := a.baseFolderId()
	for _, name := range list {
		fmt.Println(list, name, path)
		if name == "." {
			continue
		}
		list, err := a.client.Children.List(baseId).Q(fmt.Sprintf("title='%s'", name)).Do()
		if err != nil {
			return nil, err
		}
		if len(list.Items) > 0 {
			baseId = list.Items[0].Id
		} else {
			file, err := a.client.Files.Insert(&drive.File{
				Title:    name,
				MimeType: GoogleDriveMimeTypeFolder,
				Parents:  []*drive.ParentReference{&drive.ParentReference{Id: baseId}},
			}).Do()
			if err != nil {
				return nil, err
			}
			baseId = file.Id
		}
	}
	return &drive.ParentReference{Id: baseId}, nil
}
func (a *GoogleDriveSync) upload(c *FileChange, f *drive.File) error {
	storage, err := a.FolderSource(c.Source)
	if err != nil {
		return err
	}
	f.Title = filepath.Base(c.File.Path)
	parent, err := a.parentOfPath(filepath.Dir(c.File.Path))
	if err != nil {
		return err
	}
	f.Parents = []*drive.ParentReference{parent}

	d := &drive.Property{Key: "path", Value: c.File.Path}
	f.Properties = []*drive.Property{d}
	m, err := os.Open(storage.absolutePath(c.File.Path))
	if err != nil {
		return fmt.Errorf("An error occurred reading the document: %v", err)
	}
	var r *drive.File
	if c.Type() == FileChangeUpdate {
		q := a.client.Files.Update(c.File.ParentVersionFile.FileChunks[0].CustomFileId, f)
		if a.chunks {
			q = q.ResumableMedia(nil, m, 0, f.MimeType)
		} else {
			q = q.Media(m)
		}
		r, err = q.Do()
	} else {
		q := a.client.Files.Insert(f)
		if a.chunks {
			q = q.ResumableMedia(nil, m, 0, f.MimeType)
		} else {
			q = q.Media(m)
		}
		r, err = q.Do()
	}

	if err != nil {
		return fmt.Errorf("An error occurred uploading the document: %v", err)
	}
	t, err := time.Parse(time.RFC3339, r.ModifiedDate)
	if err != nil {
		return err
	}
	chunk := &FileChunk{
		ConnectedStorageId: a.ConnectedStorageId(),
		Size:               c.File.Size,
		CustomFileId:       r.Id,
		ModTime:            t,
	}
	c.File.FileChunks = append(c.File.FileChunks, chunk)
	return a.SaveFile(c.File)
}
func (g *GoogleDriveSync) Remove(fc *FileChange) error {
	for _, chunk := range fc.File.FileChunks {
		_, err := g.client.Files.Trash(chunk.CustomFileId).Do()
		if err != nil {
			return err
		}
	}
	return g.DeleteFile(fc.File)
}
