package lib

import "testing"

func TestFactoryCreate(t *testing.T) {
	var ssf ServiceSyncerFactory
	ssf = &FolderSyncFactory{}
	csc := &ConnectedStorageController{cs: &ConnectedStorage{}}
	_, err := ssf.Create(csc)
	if err == nil {
		t.Error("Path should not exist")
	}
}
