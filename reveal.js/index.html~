<!doctype html>
<html lang="en">

	<head>
		<meta charset="utf-8">

		<title>reveal.js - The HTML Presentation Framework</title>

		<meta name="description" content="A framework for easily creating beautiful presentations using HTML">
		<meta name="author" content="Hakim El Hattab">

		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">

		<link rel="stylesheet" href="css/reveal.css">
		<link rel="stylesheet" href="css/theme/white.css" id="theme">

		<!-- Code syntax highlighting -->
		<link rel="stylesheet" href="lib/css/zenburn.css">

		<!-- Printing and PDF exports -->
		<script>
			var link = document.createElement( 'link' );
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
			document.getElementsByTagName( 'head' )[0].appendChild( link );
		</script>

		<!--[if lt IE 9]>
		<script src="lib/js/html5shiv.js"></script>
		<![endif]-->
	</head>

	<body>

		<div class="reveal">
<div id="myName" style="position:absolute;bottom:20px; right:11%;font-size:50%;">Aidas Klimas, IFF-1</div>
<div id="myLogo" style="background: url(./img/ktu.jpg) bottom center no-repeat;background-size:contain;
                        position: absolute;
                        bottom: 20px;
                        right: 20px;
                        width: 9%;
                        height: 15%;"></div>

			<!-- Any section element inside of this container is displayed as a slide -->
			<div class="slides">
				<section>
					<h1 style="font-size:46pt">Debesų saugyklų sinchronizavimo sistema</h1>
          <p style="text-align:center">
          <small>Darbo vadovas: dr. Andrej Ušaniov</small><br/>
          <small>Atliko: Aidas Klimas, IFF-1</small>
          </p>
				</section>

				<section>
					<h2>Idėja</h2>
					<p>
          <img src="./img/cloudsync.png" style="background:none; border:none; box-shadow:none;">
					</p>
				</section>

        <section>
          <h2>Reikalavimai</h2>
          <ul>
            <li>Platus platformų palaikymas: Windows, Mac, Linux, ...</li>
            <li>Paprasta integruoti naujas saugyklas</li>
            <li>Sistema turi veikti kaip servisas</li>
            <li>Nuolatinis sinchronizavimas tarp saugyklų ir aplanko</li>
          </ul>
				</section>


        <section>
          <h2>Panaudojimo atvejai</h2>
          <img src="./img/usecase.png" style="background:none; border:none; box-shadow:none;">
          <p>Biblioteka pastoviai stebi vartotojo veiksmus ir vykdo sinchronizaciją.</p>
				</section>


        <section>
          <h2>Sinchronizavimo algoritmas</h2>
          <img src="./img/activity.png" style="background:none; border:none; box-shadow:none;">
				</section>

        <section>
          <h2>Programavimo kalba - GO
          <img src="./img/gologo.png" style="background:none; border:none; box-shadow:none; width:10%; vertical-align:middle"/>
</h2>
<pre style="width:49%;float:right;"><code data-trim>
defer close(progressc)
changes, err := s.GetChanges()
if err != nil {
    return err
}
for i := range changes {
  err = changes[i].Apply()
  if err != nil {
    return err
  }
  p.Value += 1
  progressc <- p
}
</code></pre>
          <ul style="width:50%;margin:0;float:left;">
            <li>Sparčiai populerėja.</li>
            <li>Platus platformų palaikymas.</li>
            <li>Paprasta rašyti kodą su lygiagretumu.</li>
            <li>Greitas kompiliavimo laikas.</li>
            <li>Daug kokybiškų atviro kodo bibliotekų.</li>
          </ul>
        </section>


        <section>
          <h2>Plėtros galimybės</h2>

          <img src="./img/class_simple.png" style="background:none; border:none; box-shadow:none;">
        </section>

        <section>
          <h2>Duomenų bazė</h2>
          <img src="./img/db.png" style="background:none; border:none; box-shadow:none;">
				</section>

        <section>
          <h2>Sistemos charakteristikos</h2>
          <table>
            <tr>
              <th>Charakteristika</th>
              <th>Kiekis</th>
            </tr>
<tr>
<td>Kodo eilučių skaičius</td>
<td>2380</td>
</tr>
<tr>
<td>Tipų skaičius</td>
<td>36</td>
</tr>
<tr>
<td>Funkcijų skaičius</td>
<td>181</td>
</tr>
<tr>
<td>Failų skaičius</td>
<td>17</td>
</tr>
<tr>
<td>Testų skaičius</td>
<td>24</td>
</tr>
<tr>
<td>Komandinės eilutės aplikacijos dydis</td>
<td>13.1MB</td>
</tr>
<tr>
<td>Aplikacijos su grafine sąsaja dydis</td>
<td>16.6MB</td>
</tr>
          </table>
        </section>

        <section>
          <h2>Testavimas</h2>
          <ul>
            <li>Naudojami vienetų ir integraciniai testai</li>
            <li>Pasiektas 63% sistemos padengiamumas.</li>
          </ul>
        </section>

        <section>
          <h5>Eksperimentas - sinchronizavimo greičio palyginimas su kitomis sistemomis</h5>

          <img src="./img/e1a.png" style="background:none; border:none; box-shadow:none;">
        </section>

        <section>
          <h5>Eksperimentas - failų įkėlimas lygiagrečiai</h5>

          <p>Failų įkėlimas lygiagrečiai</p>
          <img src="./img/kelimas4.png" style="background:none; border:none; box-shadow:none;">
        </section>

        <section>
          <h2>Ateities planai</h2>
          <ul>
            <li>Numatoma palaikyti daugiau saugyklų, tarp jų ir „OneDrive“, „Box“ ir „Copy“,</li>
            <li>Numatoma sumažinti failų sinchronizacijos laiką.</li>
            <li>Ateityje biblioteka galėtų veikti kaip virtualus diskas. Veiktų panašiu principu kaip „RAID 0“</li>
          </ul>
        </section>

        <section>
          <h2>Išvados</h2>
          <ol style="font-size:90%">
            <li>Buvo pastebėtas mažas „Go“ kalbos palaikymas, tik viena saugykla yra pateikus dokumentaciją. Norint integruoti kitas saugyklas reikia įgyvendinti saugyklų API patiems.
</li>
            <li>Atliktas eksperimentas. Buvo nustatyta, kad keliant failus į vieną saugyklą lygiagrečiai, galima sumažinti įkėlimo laiką daugiau nei 2 kartus.</li>
            <li>Buvo pastebėta, kad saugyklos turi apribojimus lygiagrečiai siunčiamiems failams. Lygiagretumo lygį reikia derinti prie saugyklos specifikos.</li>
          </ol>
        </section>

				<section style="text-align: left;">
					<h1>Ačiū už dėmesį</h1>
				</section>

			</div>

		</div>

		<script src="lib/js/head.min.js"></script>
		<script src="js/reveal.js"></script>

		<script>

			// Full list of configuration options available at:
			// https://github.com/hakimel/reveal.js#configuration
			Reveal.initialize({
				controls: false,
mouseWheel: true,
        slideNumber: true,
margin: 0.1,
				progress: true,
				history: true,
				center: true,
        minScale: 0.9,
        maxScale: 3.5,

				transition: 'slide', // none/fade/slide/convex/concave/zoom

				// Optional reveal.js plugins
				dependencies: [
					{ src: 'lib/js/classList.js', condition: function() { return !document.body.classList; } },
					{ src: 'plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
					{ src: 'plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
					{ src: 'plugin/highlight/highlight.js', async: true, condition: function() { return !!document.querySelector( 'pre code' ); }, callback: function() { hljs.initHighlightingOnLoad(); } },
					{ src: 'plugin/zoom-js/zoom.js', async: true },
					{ src: 'plugin/notes/notes.js', async: true }
				]
			});

		</script>

	</body>
</html>
